Fisky -- Experimental Orchestration of the UDKM Fifer Experiment
================================================================

[[_TOC_]]

System Architecture
-------------------

### System Overview

### Main Host Configuration

### Involved Devices & EPICS IOCs

### Data Management

### Additional Support Services


User's Guide
------------

### Login & Startup

### Hello, Fifer!

### Accessing and Analyzing Data

### Advanced Measurement Plans


Technician's Guide
------------------

### Involved Documentation and Repositories

### Develop-Test-Deploy Process

### Fixing Small Bugs

### Large and Structural Modifications


Caveats & Bugs
--------------

### Mind the Auto-Update!

### Sharp Edges

### Support

It's Yours to Keep.
