#!/usr/bin/python3

''' Namespace preparations for UDKM Fisky Runs '''

from os import environ

## Fisky specific devices
from fisky.devices import *

fisky_prefix = environ.get('FISKY_PREFIX', 'UDKM:FISKY')
oszi = TraceControl(f'{fisky_prefix}:OSZI:', name="oszi")
delay = DelayDevice(f'{fisky_prefix}:DELAY:', name="delay")
siggn = SignalDevice(f'{fisky_prefix}:SIGGN:', name="siggn")

## We do everything through the dedicated Session() object,
## which will also take care of some additional context details.
from fisky.session import Session

## Useful to allow the user to build their own peak shape
from fisky.devices.signal import (
    Peak, SquarePeak,
    PeakComponent, SquarePeakComponent, 
    Pulse
)

## Bluesky RunEngine
#from bluesky import RunEngine
#RE = RunEngine({})


## Some fancier visualisation
#from bluesky.callbacks.best_effort import BestEffortCallback
#visual_bec = BestEffortCallback()
#RE.subscribe(visual_bec)

#from bluesky.utils import ProgressBarManager
#RE.waiting_hook = ProgressBarManager()


## VERY simple storage to HDF5. For poor people only.
#from fisky.storage.hdf5 import SimpleHdf5Storage
#fisky_storage = SimpleHdf5Storage()
#RE.subscribe(fisky_storage)


## Prepare a number of plans readily available
from bluesky.plans import (
    count,
    scan,
    rel_scan,
    list_scan,
    list_grid_scan,
    grid_scan,
    tweak,
)

# For testing, you can try this:
#
#  $ cd escpid
#  $ src/escpi/application.py --from-yaml examples/keithley-3390.yaml \
#                             --resource-manager test/visa-sim-keith3390.yml@sim \
#                             --device TCPIP::keith3390-sim::INSTR \
#                             --prefix UDKM:FISKY:SIGGN:
#
#  $ cd oszitrace-ioc
#  $     OSZI_EPICS_PREFIX=UDKM:FISKY: \
#        OSZI_NAME=OSZI \
#        OSZI_SIM_CHANNELS=voltage:current:gtrig:ltrig \
#    oszitrace-ioc
#
#  $ cd kronos-ioc
#  $     KRONOS_SIM=yes \
#        DG645_EPICS_PREFIX=UDKM:FISKY:DELAY: \
#    dg645-ioc
#
#  $ cd ~/
#  $ ipython
#    In [.]: from fisky.live import *
#    In [.]: fisky_storage.file_path = '~/moo.h5'
#    In [.]: RE(count([siggn.ampl, oszi.current, delay.ch1], num=5))
#    In [.]: RE(scan( [siggn.ampl, oszi.current, delay.ch1], delay.ch1.dur, 0, 0.1, 10))
