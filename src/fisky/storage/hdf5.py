#!/usr/bin/python3

from bluesky.callbacks import CallbackBase
from pprint import pprint

import numpy as np
import h5py

class SimpleHdf5Storage(CallbackBase):
    '''
    Simple HDF5 storage callback. Stores all data flat, in the root node.

    ACHTUNG: This will do bad things!
    It will overwrite existing files.
    It will overwrite existing groups.
    It will NOT consider pre-existing runs if RE is restarted.

    It will kick your dog, f^@%$ your girlfriend, AND eat your food.

    Quite generally, it will not leave out any occasion to stab you
    in the back if you let it. Handle with care and extreme skepticism.
    
    Args:
        h5path: HDF5 file name to store data in. Data will be
          stored flat.
    '''
    
    
    def __init__(self, h5path=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._h5path = None
        self._h5node = None
        self._running = False
        self._order = None
        self.file_path = h5path


    def _get_h5obj(self, init=True):
        if self.file_path is not None:
            fpath = self.file_path
        else:
            fpath = '/tmp/fisky.h5' # for debugging
        return h5py.File(fpath, 'a')


    @property
    def file_path(self):
        return self._h5path


    @file_path.setter
    def file_path(self, val):
        if not self._running:
            self._h5path = val
        else:
            raise RuntimeError('Cannot change path while a scan is running')


    def start(self, doc):
        ''' Received at the beginning of a scan '''

        # See also: https://blueskyproject.io/bluesky/main/documents.html#run-start
        
        self._running = True
        scan_id = doc['scan_id']  # numerical ID 
        scan_uid = doc['uid']     # string UUID
        self._num_pts = doc['plan_args']['num']

        if self.file_path is None:
            print(f'ACHTUNG: h5path not set!')
            # saving to a temporary file, for debugging
        
        self._h5node = f'{scan_id}'
        with self._get_h5obj(init=False) as h5:
            try:
                # Overwrite scan-group if it exists (i.e.
                # delete the old one first)
                n = h5[self._h5node]
                print(f'ACHTUNG: Overwriting node "{self._h5node}"')
                del h5[self._h5node]
                raise KeyError()
            except KeyError:
                n = h5.create_group(self._h5node)
                print(f'Created node: "{n.name}" (in {h5.filename})')


    def descriptor(self, doc):
        '''
        Received with the first data point,
        contains data field names, types etc.
        '''
        
        # Data keys are in doc['data_keys'].
        # See also: http://blueskyproject.io/bluesky/main/event_descriptors.html
        # and: https://blueskyproject.io/bluesky/main/documents.html#event-descriptor

        with self._get_h5obj(init=True) as h5:
            h5node = h5[self._h5node]
            for key, spec in doc['data_keys'].items():
                try:
                    dt_spec = spec['dtype']
                    dtype = {
                        'number': float,
                        'integer': int,
                        'array': float,
                    }[dt_spec]
                except KeyError:
                    print(f'ERROR: {key}: Don\'t know how to save "{dt_spec}"')
                    continue

                shape = spec['shape']
                dshape = (0, *shape)
                mshape = (self._num_pts, *shape)

                try:
                    # Overwrite dataset if it exists (i.e.
                    # delete the old one first)
                    dset = h5node[key]
                    del h5node[key]
                    raise KeyError()
                except KeyError:
                    h5node.create_dataset(key, dshape, maxshape=mshape,
                                          dtype=dtype, compression='lzf')


    def event(self, doc):
        ''' Received for every data point in a scan '''
        
        # Data is in doc['data'].
        # See also: https://blueskyproject.io/bluesky/main/documents.html#event

        with self._get_h5obj(init=False) as h5:
            h5node = h5[self._h5node]
            for key, data in doc['data'].items():
                item = np.array(data)
                try:
                    dset = h5node[key]
                except KeyError:
                    continue
                
                dset.resize((dset.shape[0]+1, *(dset.shape[1:])))
                dset[-1] = item


    def stop(self, doc):
        ''' Received at the end of a scan '''
        # See also: https://blueskyproject.io/bluesky/main/documents.html#run-stop
        
        self._running = False
        self._h5node = None
