#!/usr/bin/python3

from ophyd import (
    EpicsSignal,
    EpicsSignalRO,
    PVPositioner,
    Device,
    Component
)

class DelayPositioner(PVPositioner):
    setpoint = Component(EpicsSignal, '')
    readback = Component(EpicsSignalRO, '_RBV')
    done = Component(EpicsSignalRO, '_MSTA')
    done_value = 0


class Channel(Device):
    dur = Component(DelayPositioner, "dur")
    dly = Component(DelayPositioner, "dly")
    offs = Component(DelayPositioner, "offs")
    ampl = Component(DelayPositioner, "ampl")
    pol = Component(EpicsSignal, "pol_RBV", write_pv="pol")


class Trigger(Device):
    lvl = Component(EpicsSignal, read_pv='lvl_RBV', write_pv='lvl')
    src = Component(EpicsSignal, read_pv='src_RBV', write_pv='src')
    intrate = Component(EpicsSignal, read_pv='intrate_RBV', write_pv='intrate')


class DelayDevice(Device):
    ch1 = Component(Channel, "ch1:")
    ch2 = Component(Channel, "ch2:")
    ch3 = Component(Channel, "ch3:")
    ch4 = Component(Channel, "ch4:")
