#!/usr/bin/python3

from ophyd import (
    EpicsSignal,
    EpicsSignalRO,
    PVPositioner,
    PVPositionerPC,
    SoftPositioner,
    PseudoPositioner,
    PseudoSingle,
    Device,
    Component,
    DynamicDeviceComponent
)

from ophyd.pseudopos import (
    pseudo_position_argument,
    real_position_argument
)

import numpy as np

import logging

logger = logging.getLogger(__name__)

class SignalPositioner(PVPositionerPC):
    setpoint = Component(EpicsSignal, '')
    readback = Component(EpicsSignalRO, '_RBV')

class PulseSetter(EpicsSignal):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def set(self, *a, **kw):
        #print(f'{self.name}: PS set')
        r = super().set(*a, **kw)
        #print(f'{self.name}: PS /set')
        return r

    def move(self, *a, **kw):
        #print(f'{self.name}: PS move')
        r = super().move(*a, **kw)
        #print(f'{self.name}: PS /move')
        return r


class SignalDevice(Device):
    ampl =  Component(SignalPositioner, "ampl")
    freq =  Component(SignalPositioner, "freq")
    offs =  Component(SignalPositioner, "offs")
    func =  Component(SignalPositioner, "func")
    ufunc = Component(SignalPositioner, "ufunc", settle_time=3.0)
    onoff = Component(EpicsSignal, "onoff")

    
class PeakParameter(SoftPositioner):
    '''
    Holds a value to be used as a parameter when building peaks.
    Constituent of a `Peak` class (or subclass thereof).
    '''
    def __init__(self, *args, **kwargs):
        self._init_done = False
        super().__init__(*args, **kwargs)
        self._init_done = True ## need to guard set(),
        # otherwise it will trigger endless loops in the Pulse class

    def _find_pulse(self, start_obj=None):
        if start_obj is None:
            start_obj = self
        if isinstance(start_obj, Pulse):
            return start_obj
        elif hasattr(start_obj, "parent"):
            return self._find_pulse(start_obj.parent)
        raise RuntimeError(f'Ran out of ancestors but haven\'t found a pulse')

    # .set() ends up in .move()

    def move(self, *args, **kwargs):
        #print(f'{self.name}: move')
        r = super().move(*args, **kwargs)
        #print(f'{self.name}: -move')
        if self._init_done:
            parent_pulse = self._find_pulse()
            mobj = parent_pulse.recalc_peak_data(peak_name=f"{self.name}",
                                                 hint=f"{self.name}.move")
        else:
            # Normally we're done when the recalc_peak_data() induced move
            # is done.
            # This is a workaround for the __init__() time move. It doesn't
            # actually do anything.
            mobj = r
        #print(f'{self.name}: /move done: {mobj.done}')
        return mobj


class Peak(Device):
    '''
    Abstract class to represent a single component in a multi-component `Pulse`.
    You'll generally want to subclass this and reimplement the `.make_partial()`
    member to return fully sampled function containing _only_ this one
    peak (all other sampling points being 0).

    It must only contain sub-devices of the type `PeakComponent` or subclasses
    thereof.
    '''
    
    def make_partial(self, length, offset, reach):
        '''
        This is expected to return a numpy array of the correct size
        containing data / sampling for _this_ peak. `length` is the number
        of points, `reach` is the "logical" X-coordinte of the last point.
        (The first point is always 0.0.)

        `params` is a dictionary with peak parameters.
        '''
        raise NotImplemented()


    def _gather_params(self):
        #for p in self.component_names:
        #    print(f'{self.name}: {p}={getattr(self, p).position}')
        return { p:getattr(self, p).position for p in self.component_names }


class SquarePeak(Peak):
    '''
    `Peak` class that generates a single square pulse.
    '''
    
    def make_partial(self, length, offset, reach):
        params = self._gather_params()
        rpx = (reach-offset) / (length+1)
        istart = int(round((params['start']-offset)/rpx))
        ilen = int(round((params['duration'])/rpx))
        a = np.zeros(length)
        a[istart:istart+ilen] = params['amplitude']
        #print(f'{self.name}: rpx={rpx} from={istart} to={istart+ilen} '
        #      f'value={params["amplitude"]} len={length} params={params}')
        return a


class PeakComponent(DynamicDeviceComponent):
    '''
    Base class for peak components. You'll generally want to subclass this
    because passing the correct `base_class` is essential (since the `base_class`
    will actually contain the peak-generating algorithm).
    '''
    def __init__(self, params=None, base_class=None, **kwargs):
        super().__init__(
            { k:(PeakParameter, None, { 'init_pos': v }) \
              for k,v in params.items() },
            base_class=base_class or Peak,
            **kwargs
        )


class Pulse(SignalDevice):  # Maybe directly from SignalDevice?
    '''
    Base class for the funcion / pulse. Contains functionlity
    to create the final pulse and pass it on to the hardware
    EPICS signal ("ufunc").

    See `SquarePeakComponent` for a usage example.

    The instance must end up with a `.ufunc` attribute suitable
    for setting the current sampling array to the signal generator.
    '''

    #ufunc = Component(SoftPositioner)

    # FIXME: These are read-only components, just to make the
    #        pulse function easier to read. They will get updated
    #        each time with .recalc_peak_data().
    #        We should somehow reflect their read-only status
    #        with a different base class (Soft-RO-Positioner?)
    pulse_signal = Component(SoftPositioner)
    pulse_xoffset = Component(SoftPositioner)
    pulse_xdelta = Component(SoftPositioner)

    def __init__(self, *args, length=137, offset=0.0, reach=1.0, **kwargs):
        super().__init__(*args, **kwargs)
        #self._peak_len = length
        #self._peak_offset = offset
        self._peak_reach = reach
        delta = (self._peak_reach-offset)/(length+1)
        self.pulse_xoffset.set(offset)
        self.pulse_xdelta.set(delta)
        self.pulse_signal.set(np.zeros(length))
        logger.warning(f'Volatile pulse shape *not* set, use .volatile_init()')
        #stat = self.recalc_peak_data(peak_name=None, hint="__init__")

        # After every parameter change we need to write to
        # self.ufunc. Ideally, we'd only want to write _once_
        # for all (part-)peaks, not after every peak.
        # We know that a parameter has been changed because
        # recalc_peak_data() is being called by upper classes.
        #
        # On every recalc_peak_data(), we'll mark the corresponding
        # peak as "visited". When all are visited, we'll trigger _one_
        # .ufunc update, and wait for it.
        self._peak_visited = { k:False for k,v in self._find_my_peaks().items() }


    def volatile_init(self):
        return self.recalc_peak_data(peak_name=None, hint="volatile_init")

    
    def _find_my_peaks(self):
        my_peaks = {}
        for name in self.component_names:
            obj = getattr(self, name)
            if issubclass(type(obj), Peak):
                my_peaks[name] = obj
        return my_peaks


    def recalc_peak_data(self, peak_name=None, hint=None, move=True, set_dict=None):
        '''
        Recalculates new peak. Moves `ufunc` if `move` is True.
        Returns MoveStatus object of the `ufunc` move.
        '''
        #print(f'{self.name}: RECALC TIME! (trigger by {peak_name}, hint: {hint})')
        full_peak = np.zeros(len(self.pulse_signal.position))
        plen = len(full_peak)
        preach = self.pulse_xoffset.position+self.pulse_xdelta.position*(plen+1)
        
        for pname,pobj in self._find_my_peaks().items():
            part = pobj.make_partial(plen, self.pulse_xoffset.position, preach)
            #print(f'{self.name}: part={pname}')
            full_peak += part

        #print(f'{self.name}: full peak, shape={full_peak.shape} sum={full_peak.sum()}, hint={hint}')

        try:
            self.pulse_signal.set(full_peak)
            ss = self.ufunc.set(full_peak)
            #print(f'{self.name}: UFUNC TIME! {ss}')
            #print()
            return ss

        except Exception as e:
            print(f'Oops: {e}.')
            raise

    def set(self, *a, **kw):
        #print(f'{self.name}: set')
        super().set(*a, **kw)
        #print(f'{self.name}: /set')

    def move(self, *a, **kw):
        #print(f'{self.name}: move')
        super().move(*a, **kw)
        #print(f'{self.name}: /move')



class SquarePeakComponent(PeakComponent):
    '''
    To be used in a `Peak` device.

    Example:
    ```
    # define your own pulse function
    class MyCustomFunction(Pulse):
        square1 = SquarePeakComponent(start=0.0, duration=0.5, amplitude=1.0)
        square2 = PeakComponent(params={'start': 0.5,
                                        'duration': 0.5,
                                        'amplitude': -1.0},
                                base_class=SquarePeak)

    # instantiate it in your experiment
    func = MyCustomFunction(length=512,  # number of sampling points
                            readh=3.14)  # the "logical" coordinate of the far end

    # then use it, e.g. scan over the start parameter
    RE( scan([...], func.peak1.start, ...) )
    ```
    '''
    def __init__(self, start=0.0, amplitude=0.0, duration=0.0, **kwargs):
        super().__init__(params={
            'start': start,
            'amplitude': amplitude,
            'duration': duration
        }, base_class=SquarePeak, **kwargs)


class TestPulse(Pulse):
    a = SquarePeakComponent(start=1.0, amplitude=1.1, duration=1.2)
    b = SquarePeakComponent(start=2.0, amplitude=2.1, duration=2.2)
