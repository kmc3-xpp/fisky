from ophyd import (
    EpicsSignal,
    EpicsSignalRO,
    PVPositioner,
    Device,
    Component
)

from ophyd.pseudopos import (
    PseudoPositioner,
    PseudoSingle,
    pseudo_position_argument,
    real_position_argument
)

class Channel(Device):
    '''
    Represents a specific signal (trace).
    
    Each channel has their own axis (technically), but actually,
    the main channels all share the same X base.
    '''
    signal = Component(EpicsSignalRO, "signal")
    xdelta = Component(EpicsSignalRO, "xdelta")
    xoffset = Component(EpicsSignalRO, "xoffset")

    
class TraceControl(Device):
    '''
    Controls the whole tracing device (a.k.a. oscilloscope).
    This is essentially just a collection of channels.
    We name the channels here explicitly after this specific
    application's needs.
    '''
    
    arm = Component(EpicsSignal, 'arm')
    voltage = Component(Channel, "voltage:")
    current = Component(Channel, "current:")
    gtrig = Component(Channel, "gtrig:")
    ltrig = Component(Channel, "ltrig:")
