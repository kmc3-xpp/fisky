#!/usr/bin/python3

# Session management -- example only

from fisky.storage.hdf5 import SimpleHdf5Storage
import h5py

import logging, os
logger = logging.getLogger(__name__)

from bluesky import RunEngine

import yaml

import numpy as np
import xarray as xr

__all__ = [ "Session" ]

# This is used/raises raised internally within Session, when trying to retrieve
# data from HDF5 files as Xarrays. To make things confortable, we try to reconstruct
# as much as possible (scaling, attributes etc). When we fail because we assume
# a specific type of layout which isn't the case, we raise this so higher layers
# know to try a different method.
class InvalidScanDataLayout(Exception): pass


class SessionH5LikePortal:
    '''
    Hides/facilitates/enforces access to data pertaining
    to a specific session. The main scope is to actually
    force the user to use a `with...` block when opening
    data for reading, as there might still be other
    processes trying to write.

    Also, we want to deliberately obscure the fact whether
    we're really using a HDF5 file, or another HDF5-like
    access interface.
    '''

    def __init__(self, session, scan="last"):
        self._session = session
        self._scan = scan
        self._cnt = 0


    def __enter__(self):
        ''' `scan` can be a number, "last", or "all" '''
        if self._cnt > 0:
            self._cnt += 1
        else:
            self._cnt = 1
            self._h5like = h5py.File(self._session.data_file, 'r')
            self._h5like.__enter__()

        if (hasattr(self._scan, "__len__") and len(self._scan) == 0) \
           or (self._scan is None):
            return self._h5like
        
        if self._scan == "last":
            sid = f'{self._session.last_scan_id}'
        else:
            sid = f'{self._scan}'
        return self._h5like[sid]


    def __exit__(self, *args, **kw):
        if self._cnt <= 0:
            return
        else:
            self._cnt -= 1

        if self._cnt == 0:
            self._h5like.__exit__()
        

class Session:

    def __new__(cls, *args, **kw):
        if not hasattr(cls, 'instance'):
            cls.instance = super().__new__(cls)
        return cls.instance


    def __init__(self, label=None, re_dict=None):
        '''
        Initializes a Fisky session.

        The main task of this is to abstract away boilerplate code of figuring out
        where the data is to be stored, retrieve any previously stored data,
        loading previous state (first and foremost the scan ID), and use it
        to create a newly usable `bluesy.RunEngine`.

        Args:
            label: unique string identifyer for the session. Note that this will
              be transformed to lower-case. The data files are generally stored under
              '{base_dir}/{label}/{label}.h5' for the measurements,
              and '.../{label}.meta' for the metadata.

            re_dict: dictionary containing additional RunEngine metadata
        '''
        
        self._label = (label or os.environ.get('FISKY_SESSION_LABEL', '')).lower()
        if len(self._label) == 0:
            logger.warning(f'session=scratch desc="Session label is not specified, '
                           f'and FISKY_SESSION_LABEL is not set; using default"')
            self._label = "scratch"
            
        self._base_dir = '/home/jovyan/data'
        self._storage_path = f'{self._base_dir}/{self._label}/{self._label}.h5'
        self._storage_callback = SimpleHdf5Storage(self._storage_path)
        self._meta_path = f'{self._base_dir}/{self._label}/{self._label}.meta'
        self._run_meta = self._load_meta(self._meta_path)
        self._re_dict = re_dict or {}

        logger.info(f'session={self._label} desc="last scan ID is {self.last_scan_id}"')


    def _load_meta(self, fpath):
        try:
            with open(fpath, 'r') as f:
                return yaml.safe_load(f)
        except yaml.YAMLError as e:
            logger.error(f'Cannot load metadata context {f}: {e}')
        except FileNotFoundError:
            logger.warning(f'session={self._label} desc="Metadata for does not exist" path={fpath}')
            pass
        return {}


    def _make_run_engine(self):
        '''
        Creates a new RunEngine instance, based on the metadata
        found at this session's storage path.
        '''
        
        rd = self._re_dict.copy()
        #self._run_meta.update({
        #    'scan_id': self.last_scan_id
        #})
        rd.update({
            'md': self._run_meta,
            'scan_id_source': self.next_scan_id,
            'call_returns_result': True
        })
        
        RE = RunEngine(**rd)
        
        RE.subscribe(self._storage_callback)

        return RE


    @property
    def RE(self):
        ''' Returns a `bluesky.RunEngine` instance to work with. '''
        if not hasattr(self, "_run_engine"):
            self._run_engine = self._make_run_engine()
        return self._run_engine

    
    def next_scan_id(self, md=None):
        if md is None:
            md = self._run_meta
        last = md.get('scan_id', self.last_scan_id)
        i = last + 1
        #logger.info(f'Creating next scan ID: {i} (from {last}, in HDF5 is {self.last_scan_id})')
        return i
        

    @property
    def last_scan_id(self):
        ''' The numerical ID of the scan that was last created. '''
        try:
            with h5py.File(self.data_file, 'r') as h5:
                return max([int(k) for k in h5.keys()])
        except Exception as e:
            logger.warning(f'desc="Cannot get last scan ID from HDF file, '
                           f'assuming 0" reason="{e}"')
            return 0


    @property
    def data_file(self):
        return self._storage_callback.file_path


    def as_h5like(self, scan):
        '''
        Returns a portal to the specified scan as a HDF5-like object.
        Note that the portal object itself isn't HDF5-like. You need
        to enclose it in `with session.as_h5like(...) as h5like: ...`
        in order to actually access the data.
        '''
        return SessionH5LikePortal(session=self, scan=scan)


    @property
    def last_h5like(self):
        return self.as_h5like(scan=self.last_scan_id)


    @property
    def all_h5like(self):
        return self.as_h5like(scan=None)


    def _load_array_from_h5like(self, h5like, key, index_dim="index"):
        '''
        Returns a tupe: (name, dimension names, data array, extra data),
        where:
          - name is the name of the dataset (usually the same as "key")
          - dimension name is a list of the dimensions, starting with
            `index_dim` as the 0-th dimension
          - data array is a numpy array with numbers (i.e. the data)
          - extra data is a dictionary with contains other datasets
            to be included, possibly related to this one. This is usually
            relevant when new dimensions are to be created.

        The information is retrieved from a h5like-object (e.g. a
        group).

        This assumes that the data is a "regular", boring, numpy-array
        without any knowledge of specific dimensions.
        '''
        dobj = h5like[key]
        dims = tuple(
            [index_dim] + [f'dim_{i}' for i in range(1, len(dobj.shape))]
        )

        return key, dims, dobj[()], {}


    def _load_cdf_from_h5like(self, h5like, key, index_dim="index"):
        pass


    def _load_signal_from_h5like(self, h5like, key, index_dim="index"):
        '''
        This does the same as `._load_array_from_h5like()`, but makes
        the following assumptions about the nature of the data.

        This follows an internal convention, checking whether if the
        dataset name (i.e. `key`) ends on`"_signal"`. If it does,
        similarly named datasets which end on `"_{x|y|z}offset"`,
        respectively `"_{x|y|z}delta"` are searched -- one for each
        dimension of the original dataset *beyond* the first dimension
        (which is the frame index).

        If there are exactly as many combinations as there are extra
        dimensions, then the letter (x, y, or z) is used as a dimension
        name for the data set, and the data is used as a coordinate axis.

        For reference, this roughly follows an "Wavemetrics IgorPro(tm)"-like
        approach to data storage, where intrinsic coordinates are stored
        as a delta/offset value.
        '''
        dobj = h5like[key]
        
        # Treat _signal, _?offset or _?delta the same way.
        
        # The tricky part is not selecting offset/delta; it's EXCLUDING
        # offset/deltas as datasets in their own right if a _signal is
        # already present.
        #
        # In order to do that, we always hunt down the _signal dataset
        # regardless of whether we've received a request for _signal,
        # _xoffset, _ydelta, ... whatever.
        #
        # As soon as anything fails, we just fall back to
        # load_array_from_h5like().

        # Usually, we will have as many offsets as we have frames, because
        # that's the way data recording works with this type of data.
        # But we don't want that. Actually, we _need_ all offsets/deltas
        # for a specific dimension to be the same. So if they're arrays,
        # we return the mean(), if it's the same. Otherwise we get loud.
        def get_one_if_all_close(ar):
            if len(ar.shape) == 0:
                return ar
            if len(ar.shape) > 1:
                logger.error(f'desc="Invalid offset/delta array shape" shape={ar.shape}')
                raise InvalidScanDataLayout()
                
            if ar.shape[0] == 1:
                return ar[0]
                
            m = ar.mean()
            diff = (ar-m)
            dev = (diff*diff).sum()
            if np.isclose(dev, 1e-15):
                return m
            logger.error(f'desc="Elements are not all similar')
            raise InvalidScanDataLayout()

        if key.endswith('_signal'):
            signal_name = key
        elif key.endswith('offset'):
            signal_name = f'{key[:-8]}_signal'
        elif key.endswith('delta'):
            signal_name = f'{key[:-7]}_signal'
        else:
            #return self._load_array_from_h5like(h5like, key, index_dim)
            raise InvalidScanDataLayout()

        # From here on, proceed as if we had received a request for ..._signal.
        # This means that we go through all the motions to retrieve all
        # the _{dim}offset / _{dim}delta values. This will be a bit repetitive,
        # but it's the least complicated.
        #
        # We'll just reject duplicate entries higher up the foodchain.

        #print(f'{key}: Hunting down signal, now known as {signal_name}')
        _key = signal_name
    
        extra_dims = 'xyz' # maximum 3, _exactly_ in this order
        extra_coords = {}
        return_data = []
        

        #print(f'{_key}: Shape {dobj.shape}, have {len(dobj.shape)-1} '
        #      f'extra dimensions ({extra_dims[:len(dobj.shape)-1]})')

        for i in range(1,len(dobj.shape)):
            try:
                dim_letter = extra_dims[i-1]
            except IndexError:
                logger.warn(f'desc="Failing to recognize the {i}-th dimension -- can only work with: i+{extra_dims}"')
                # Careful here, this is the real "key". not "_key". This is
                # because at this point it turns out tha we were wrong all along
                # in assuming that "key" was part of a "signal/offset/delta"
                # thing.
                raise InvalidScanDataLayout()
                #return self._load_array_from_h5like(h5like, key, index_dim)
                
            #print(f"{_key}/{dim_letter}: Looking for dimension...")
            dim_name = _key.replace("_signal", f"_{dim_letter}")
            offs_key = _key.replace("_signal", f"_{dim_letter}offset")
            delt_key = _key.replace("_signal", f"_{dim_letter}delta")
            try:
                offs = get_one_if_all_close(h5like[offs_key][()])
                delt = get_one_if_all_close(h5like[delt_key][()])
            except KeyError as e:
                logger.warn(f'key={_key} dim={dim_letter} desc="dimension does not exist, {e}"')
                raise InvalidScanDataLayout()
                #return self._load_array_from_h5like(h5like, key, index_dim)
                
            #print(f'{_key}/{dim_letter}: offset={offs}, delta={delt}')
            
            coords = np.linspace(offs, offs+delt*dobj.shape[i],
                                 num=dobj.shape[i], endpoint=False)
            #print(f'{_key}/{dim_letter}: Coords shape: {coords.shape}')
            extra_coords[dim_name] = coords
        
        dims = tuple(
            [index_dim] + [k for k in extra_coords.keys()]
        )

        # Remove _signal from the signal name
        return _key[:-7], dims, dobj[()], extra_coords


    def as_xarray(self, scan, use_dask=False, index_dim='index'):
        '''
        Returns all the data of the specified scan as an `xarray.Dataset`.
        The procedure does its best to reconstruct dimensions names,
        but all it can really do is set the first dimension to an
        index name (`index_dim`, "index" by default) and enumerate all
        remaining dimensions.

        We're trying to load real Xarray/NetCDF-like data, if the data
        structure in the HDF5-like is NetCDF-like.
        '''
        
        with self.as_h5like(scan) as h5:
            first_key = next(iter(h5.keys()))
            first_data = h5[first_key]
            
            ds = xr.Dataset(
                coords={
                    index_dim: np.array(range(first_data.shape[0]))
                }
            )
            
            for k in h5.keys():
                try:
                    name, dims, data, extra = self._load_signal_from_h5like(h5, k, index_dim)
                except InvalidScanDataLayout:
                    name, dims, data, extra = self._load_array_from_h5like(h5, k, index_dim)
                
                if (name is not None):
                    if (name not in ds):
                        ds[name] = (dims, data)
                    else:
                        logger.warn(f'key={k} desc="Ignoring, duplicate under a different name" name={d}')
                else:
                    logger.warn(f'key={k} desc="Ignoring unnamed dataset"')

                if len(extra) > 0:
                    for d,v in extra.items():
                        if d not in ds:
                            ds[d] = v
                        else:
                            logger.warn(f'key={k} desc="Ignoring, already present"')
            
            return ds
        
        
    @property
    def last_xarray(self):
        return self.as_xarray(self.last_scan_id)