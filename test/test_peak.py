from fisky.devices.signal import Peak, SquarePeak, PeakParameter
from bluesky import RunEngine
from bluesky import plans

from pprint import pprint

def test_peak():

    RE = RunEngine({})
    sq = SquarePeak(name="square")

    def show_stuff(*args, **kw):
        pprint(args)
        pprint(kw)

    sq.start.set(3.14)
    sq.duration.set(0.5)
    sq.amplitude.set(1.0)

    print('Measuring...')

    RE(plans.count([sq]), show_stuff)
